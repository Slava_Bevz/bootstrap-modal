let [...card] = document.getElementsByClassName("card");
let [...btn] = document.getElementsByClassName("card-btn");

function getOS() {
    let platform = window.navigator.platform,
        mac = ["Macintosh", "MacIntel", "MacPPC", "Mac68K"],
        windows = ["Win32", "Win64", "Windows", "WinCE"],
        os = null;
  
    if (mac.indexOf(platform) !== -1) {
        os = "Mac OS";
        card.forEach((e) => {
            e.style.background = "#BB4844";
            e.style.color = "#FEFEFE";
        })
        btn.forEach((e) => {
            e.style.background = "#FEFEFE";
            e.style.color = "#FEFEFE";
        })
    } else if (windows.indexOf(platform) !== -1) {
        os = "Windows";
        card.forEach((e) => {
            e.style.background = "#4A8AF4";
            e.style.color = "#FEFEFE";
        })
        btn.forEach((e) => {
            e.style.borderColor = "#FEFEFE";
            e.style.color = "#FEFEFE";
        })
    } else if (!os && /Linux/.test(platform)) {
        os = "Linux";
        card.forEach((e) => {
            e.style.background = "green";
            e.style.color = "#FEFEFE";
        })
        btn.forEach((e) => {
            e.style.background = "#FEFEFE";
            e.style.color = "#FEFEFE";
        })
    }
    return os;
}
getOS();

navigator.getBattery().then(function(battery) {
    let level = battery.level;
    let levelBattery = level * 100;
    if (levelBattery <= 95){
        document.querySelector('[data-target="#exampleModal"]').click();
    }
});

